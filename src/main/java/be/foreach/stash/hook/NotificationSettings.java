package be.foreach.stash.hook;

/**
 * @author ND
 * @since 16/11/2014
 */
public abstract class NotificationSettings {
    // General
    public static final String FIELD_BRANCH_FILTER = "branchFilter";
    public static final String ENABLE_MAIL_NOTIFICATION = "mailNotification";
    public static final String ENABLE_SLACK_NOTIFICATION = "slackNotification";
    public static final String ERROR_NOTIFICATION_TYPES = "notificationTypesError";

    // Mail notifications
    public static final String FIELD_MAIL_TO = "mailToAddress";
    public static final String FIELD_MAIL_OVERRIDES_FROM = "mailOverridesFrom";
    public static final String FIELD_MAIL_OVERRIDES_SUBJECT = "mailOverridesSubject";
    public static final String FIELD_MAIL_OVERRIDES_BODY = "mailOverridesBody";
    public static final String TEMPLATE_FROM = "mailFromTemplate";
    public static final String TEMPLATE_SUBJECT = "mailSubjectTemplate";
    public static final String TEMPLATE_BODY = "mailBodyTemplate";

    // Slack notifications
    public static final String FIELD_SLACK_CHANNELS = "slackChannels";
    public static final String FIELD_SLACK_WEB_HOOK_URL = "slackWebHookUrl";


}
